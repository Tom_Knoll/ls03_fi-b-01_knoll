package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;
import java.awt.Color;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 * @param source 
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, User user, String source) {
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnPlayLevel = new JButton("Spielen");
		btnPlayLevel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnPlayLevelClicked(alienDefenceController, user);
			}
		});
		pnlButtons.add(btnPlayLevel);
		
		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

		JButton btnShowHighscore = new JButton("Highscore anzeigen");
		btnShowHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnShowHighscoreClicked(alienDefenceController);
			}
		});
		pnlButtons.add(btnShowHighscore);
		
		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);
		
		tblLevels.setForeground(Color.orange);
		tblLevels.setBackground(Color.black);
		tblLevels.setFillsViewportHeight(true);
		tblLevels.getTableHeader().setBackground(Color.black);
		tblLevels.getTableHeader().setForeground(Color.orange);
		tblLevels.setSelectionBackground(Color.gray);
		tblLevels.setSelectionForeground(Color.orange);
		
		this.setBackground(Color.black);
		
		this.updateTableData();
		
		if (source.equals(Command.Testen.toString())) {
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
			btnShowHighscore.setVisible(false);
		} else if (source.equals(Command.Leveleditor.toString())) {
			btnPlayLevel.setVisible(false);
			btnShowHighscore.setVisible(false);
		} else if (source.equals(Command.Highscore.toString())) {
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
			btnPlayLevel.setVisible(false);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}
	
	private void btnPlayLevelClicked(AlienDefenceController alienDefenceController, User user) {
		// if no row selected
		if (this.tblLevels.getSelectedRow() == -1) {
			return;
		}
		
		int levelId = Integer.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		Level level = alienDefenceController.getLevelController().readLevel(levelId);
		
		Thread t = new Thread("GameThread") {
			@Override
			public void run() {
				GameController gameController = alienDefenceController.startGame(level, user);
				new GameGUI(gameController).start();
			}
		};
		t.start();
		this.leveldesignWindow.dispose();
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	
	private void btnShowHighscoreClicked(AlienDefenceController alienDefenceController) {
		// if no row selected
		if (this.tblLevels.getSelectedRow() == -1) {
			return;
		}
		
		int levelId = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		Level level = new Level(levelId);
		new Highscore(alienDefenceController.getAttemptController(), level);
	}
}
