package controller;

import model.User;
import model.persistence.IUserPersistence;
import model.persistenceDB.AccessDB;
import model.persistenceDB.UserDB;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistence userPersistance;
	
	public UserController(IUserPersistence userPersistance) {
		this.userPersistance = userPersistance;
	}
	
	/**
	 * f�gt einen neuen User der Datenbank hinzu, falls dieser noch nicht existiert
	 * @param user der hinzuzuf�gende User
	 */
	public void createUser(User user) {
		AccessDB accessDB = new AccessDB();
		UserDB userDB = new UserDB(accessDB);
		if (userDB.readUser(user.getLoginname()) != null) {
			userDB.createUser(user);
		}
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		User user = userPersistance.readUser(username);
		
		if (user != null) {
			return user;
		}
		return null;
	}
	
	/**
	 * TODO implement
	 * @param user
	 */
	public void changeUser(User user) {
		
	}
	
	/**
	 * l�scht einen User aus der Datenbank
	 * @param user der zu l�schende User
	 */
	public void deleteUser(User user) {
		AccessDB accessDB = new AccessDB();
		UserDB userDB = new UserDB(accessDB);
		userDB.deleteUser(user);
	}
	
	/**
	 * �berpr�ft das Passwort zu dem username
	 * @param username
	 * @param passwort
	 * @return true, wenn das Passwort f�r den Username �bereinstimmt
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = readUser(username, passwort);
		
		if (user == null) {
			return false;
		}
		
		if (user.getPassword().equals(passwort)) {
			return true;
		}
		return false;
	}
}
